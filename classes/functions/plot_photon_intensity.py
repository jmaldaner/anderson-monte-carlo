import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from classes.functions.constants import *


def plot_photon_intensity(x,y,intense, name, title):
     
    r = np.max(np.max(x))/micro
    fig = plt.figure()
    cax = plt.imshow(np.log(np.add(intense,1)), cmap = 'magma', interpolation = 'nearest',vmin=np.min(np.max(intense)), vmax=np.max(np.max(intense)) - np.min(np.max(intense)), extent=(r,-r,-r,r))
    
    plt.xlabel('\micro m')
    plt.ylabel('\micro m')
    cbar = fig.colorbar(cax)
    plt.title(title)
    plt.savefig(name + '.png')
    plt.close()


def plot_transverse_intensity(x, intense, name, title):
    fig = plt.figure()
    plt.scatter(x,intense, s=1)
    plt.ylabel('Absorbed')
    plt.xlabel('z [m]')
    plt.yscale('log')
    plt.ylim(10**-9,10)
    plt.title(title)
    plt.savefig(name + '.png')
    plt.close()
