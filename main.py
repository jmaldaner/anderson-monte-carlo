from classes.space import *
from classes.materials import *
from classes.photonpacket import *
from classes.sources import *

from classes.functions.get_args import *
from classes.functions.pickle_variables import *
from classes.functions.unpickle_variables import *
from classes.functions.plot_photon_intensity import *

space_config = 'space-rmax125-smin02-1000-l001-nz100'
materials_config = 'materials-R640EthanolCore-SilicaClad'
fibreprofile_config = 'fibre-profile-g-ALOF-1000'
pump_config = 'pump-origin-8umdr-0001deg'

#space_config = 'space-rmax250-smin15-10'
#materials_config = 'materials-R640EthanolCore-SilicaClad'
#fibreprofile_config = 'fibre-profile-simple-10'
#pump_config = 'pump-origin-8umdr-0001deg'

space_data, material_data, profile_data, pump_data, n  = get_args(space_config, materials_config, fibreprofile_config, pump_config)




space1 = Space( data = space_data)

core = material_data.n0
clad = material_data.n1
air = material_data.n2


materials = [core, clad, air]
space1.material_creation( profile_data.m1, materials)

pump = Pump(x = -00*micro,data = pump_data)
spont = Spontaneous_object(core)
stimulated = Stimulated_object(core)

#space1.pump_space(pump, n)
space1.import_pump("../data/December/03/09-00",10**9)
#loaded = space1.store_load()
space1.initiate_emission(spont, stimulated, n)


loaded = space1.store_load()









# https://www.python.org/dev/peps/pep-0008/
