import numpy as np
import matplotlib.pyplot as plt
import pickle
from mpl_toolkits.mplot3d import Axes3D
from classes.functions.plot_photon_intensity import plot_photon_intensity, plot_transverse_intensity


class unpickle_variables(object):
    def __init__(self, folder):
        print("-"*100)
        print("Loading Data")
        self.folder = folder + "/"
        self.name = self.folder.replace("/","-")
        self.name = self.name.replace(".-data","")
        self.name = self.name.replace(".-","")
        self.zlin = np.load(self.folder + "zlin.npy")
        self.xlin = np.load(self.folder + "xlin.npy")
        self.ylin = np.load(self.folder + "ylin.npy")
        
        
        space_sources = open(self.folder + "space_dict.pickle", "rb")
        self.sources = pickle.load(space_sources)
        space_sources.close()
        
        self.absorbed = {}
        self.end_intensity = {}
        self.front_intensity = {}
        self.side_intensity = {}
        
        self.source_intensity = {}
        
        for key, value in self.sources.items():
            self.absorbed[key] = np.load(self.folder + key + "_absorbed.npy")
            self.end_intensity[key] = np.load(self.folder + key + "_End-Intensity.npy")
            
            self.front_intensity[key] = np.load(self.folder + key + "_Front-Intensity.npy")
            self.side_intensity[key] = np.load(self.folder + key + "_Side-Intensity.npy")
            self.source_intensity[key] = np.load(self.folder + key + "_Source-Intensity.npy")
        if "Spontaneous" in self.sources:
            self.all_spont_absorbed = np.load(self.folder +  "All_Spontaneous_absorbed.npy")
            self.all_spont_end_intensity = np.load(self.folder + "All_Spontaneous_End-Intensity.npy")
            self.all_spont_front_intensity = np.load(self.folder + "All_Spontaneous_Front-Intensity.npy")
            self.all_spont_side_intensity = np.load(self.folder + "All_Spontaneous_Side-Intensity.npy")
        if "Stimulated" in self.sources:
            self.all_stim_absorbed = np.load(self.folder +  "All_Stimulated_absorbed.npy")
            self.all_stim_end_intensity = np.load(self.folder + "All_Stimulated_End-Intensity.npy")
            self.all_stim_front_intensity = np.load(self.folder + "All_Stimulated_Front-Intensity.npy")
            self.all_stim_side_intensity = np.load(self.folder + "All_Stimulated_Side-Intensity.npy")
        self.m = np.load(self.folder + "Material-Type.npy")
        self.DeltaN = np.load(self.folder + "DeltaN.npy")
        self.E_array = np.load(self.folder + "E_array.npy")
        self.E_list = np.load(self.folder + "E_list.npy")
        self.E_weight = np.load(self.folder + "E_weight.npy")

        
        self.x, self.y, self.z = np.meshgrid(self.xlin, self.ylin, self.zlin)
        
    def plot_data(self):
        print("-"*100)
        print("Plotting Data")
        file = self.folder + self.name
        for key, value in self.sources.items():
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.source_intensity[key] ,file +  key + "_source_intensity", key + ' Exit End ' + self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.end_intensity[key] ,file +  key + "_end_intensity", key + ' Exit End ' + self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.front_intensity[key] ,file +  key + "_front_intensity", key + ' Exit Front '+ self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.side_intensity[key] ,file +  key + "_side_intensity", key + ' Exit Side '+ self.name)
            plot_transverse_intensity(self.z[0,0,:], self.absorbed[key], file + key + "_absorbed_abs",key + ' Absorbed Absolute '+ self.name)
            if sum(self.absorbed[key]) !=0:
                plot_transverse_intensity(self.z[0,0,:], self.absorbed[key]/sum(self.absorbed[key]), file + key + "_absorbed_rel",key + ' Absorbed Relative '+ self.name)
        plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.m ,file + "material-type", 'Materials '+ self.name)
        plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.E_array ,file + "energy", 'Absorbed Energy '+ self.name)
        if "Spontaneous" in self.sources:
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_spont_end_intensity ,file +  "All_Spontaneous_end_intensity", 'All Spontaneous Exit End ' + self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_spont_front_intensity ,file +  "All_Spontaneous_front_intensity", 'All Spontaneous Exit Front '+ self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_spont_side_intensity ,file +  "All_Spontaneous_side_intensity", 'All Spontaneous Exit Side '+ self.name)
            plot_transverse_intensity(self.z[0,0,:], self.all_spont_absorbed, file + "All_Spontaneous_absorbed_abs",'All Spontaneous Absorbed Absolute '+ self.name)
            if sum(self.all_spont_absorbed) !=0:
                plot_transverse_intensity(self.z[0,0,:], self.all_spont_absorbed/sum(self.all_spont_absorbed), file +"All_Spontaneous_absorbed_rel",'All Spontaneous Absorbed Relative '+ self.name)
        if "Stimulated" in self.sources:
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_stim_end_intensity ,file +  "All_Stimulated_end_intensity", 'All Stimulated Exit End ' + self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_stim_front_intensity ,file +  "All_Stimulated_front_intensity", 'All Stimulated Exit Front '+ self.name)
            plot_photon_intensity(self.x[:,:,0],self.y[:,:,0],self.all_stim_side_intensity ,file +  "All_Stimulated_side_intensity", 'All Stimulated Exit Side '+ self.name)
            plot_transverse_intensity(self.z[0,0,:], self.all_stim_absorbed, file + "All_Stimulated_absorbed_abs",'All Stimulated Absorbed Absolute '+ self.name)
            if sum(self.all_stim_absorbed) !=0:
                plot_transverse_intensity(self.z[0,0,:], self.all_stim_absorbed/sum(self.all_stim_absorbed), file +"All_Stimulated_absorbed_rel",'All Stimulated Absorbed Relative '+ self.name)
