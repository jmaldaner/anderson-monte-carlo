__author__ = "James Maldaner"

import time
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from classes.functions.print_progress import print_progress
from classes.functions.plot_photon_intensity import plot_photon_intensity
from classes.functions.find_nearest import find_nearest
from classes.functions.timer import timer
from classes.functions.constants import *


from classes.photon import Photon

random = np.random.uniform
gauss = np.random.normal


class PhotonPacket(object):
    def __init__(self, n_photons = 100):
        self.n_photons = n_photons
        self.photon_data = np.zeros((self.n_photons,7))
        self.t = timer() 
        
    def loop_photon(self, space, source, photon_num):
        p = Photon(space, source, photon_num)
        while p.status:
            self.deaths[photon_num] = p._loop_photon(space)
            self.diagnose_death(p, space)
        self.photon_data[photon_num, 0] = photon_num
        self.photon_data[photon_num, 1] = p.count
        self.photon_data[photon_num, 2] = p.death
        self.photon_data[photon_num, 3] = p.x
        self.photon_data[photon_num, 4] = p.y
        self.photon_data[photon_num, 5] = p.z
        self.t.report()
        self.photon_data[photon_num, 6] = self.t.time_raw
            
    def diagnose_death(self, photon, space):
        if self.photon_count != 0:
            if photon.death == 1:
                #print("1")
                self.end_intensity[photon.idy, photon.idx] +=photon.w / self.photon_count

            elif photon.death == 2:
                theta = np.arctan(photon.y/photon.x) + pi*(abs(photon.y) - photon.y)/(2*photon.y)
                photon.x = space.r_max * np.cos(theta)
                photon.y = space.r_max * np.sin(theta)
                photon.idx = find_nearest(space.xlin, photon.x)
                photon.idy = find_nearest(space.ylin, photon.y)
                self.side_intensity[photon.idy, photon.idx] +=photon.w/ self.photon_count 
                #print("2")

            elif photon.death == 3:
                #print("3")
                pass
            elif photon.death == 4:
                self.front_intensity[photon.idy, photon.idx] +=photon.w/ self.photon_count
                #print("4")

        


    
    def packet_death(self, space):
        self.unique, self.counts = np.unique(self.deaths, return_counts = True)
        if self.n_photons != 0:
            space.DeltaN /=  self.n_photons
        for k in range(space.nz):
            self.absorbed[k] = sum(sum(space.DeltaN[:,:,k]))

        while len(self.unique) < 4:
            self.unique = np.append(self.unique, 0)
            self.counts = np.append(self.counts,0)

        
        
 
            
    def launch_pump_photons(self, space, source):
        self.deaths = np.zeros((self.n_photons,1))
        self.end_intensity = np.zeros(np.shape(space.space_size))
        self.side_intensity = np.zeros(np.shape(space.space_size))
        self.absorbed = np.zeros(np.shape(space.zlin))
        self.front_intensity = np.zeros(np.shape(space.space_size))
        self.photon_count = self.n_photons
        p = print_progress(self.n_photons)
        
        for i in range(self.n_photons):

            self.loop_photon(space, source, i)
            p.write_percent(i/self.n_photons)

        p.clear()
        self.source_intensity = source.source_intensity
        
        self.n_photons = self.photon_count
    
        self.packet_death(space)
        space.clock.report()
        
    def launch_spont_photons(self, space, source):
        self.deaths = np.zeros((self.n_photons,1))
        self.end_intensity = np.zeros(np.shape(space.n))
        self.side_intensity = np.zeros(np.shape(space.n))
        self.absorbed = np.zeros(np.shape(space.zlin))
        self.front_intensity = np.zeros(np.shape(space.n))
        self.max_n_photons = self.n_photons
        self.photon_count = 0
        
        
       
        
        self.spontanisuly_emmit(space,source)
        

        self.source_intensity = source.source_intensity
        
        self.packet_death(space)
        
        self.n_photons = self.photon_count
        if self.n_photons == 0: self.n_photons = 1
        space.clock.report()
    def launch_stim_photons(self, space, source):
        self.deaths = np.zeros((self.n_photons,1))
        self.end_intensity = np.zeros(np.shape(space.n))
        self.side_intensity = np.zeros(np.shape(space.n))
        self.absorbed = np.zeros(np.shape(space.zlin))
        self.front_intensity = np.zeros(np.shape(space.n))
        self.max_n_photons = self.n_photons
        self.photon_count = 0
        
        self.stimulated_emmit(space,source)
        
        self.n_photons = self.photon_count
        if self.n_photons == 0: self.n_photons = 1
        self.source_intensity = source.source_intensity
        self.packet_death(space)
        space.clock.report()
        
    def spontanisuly_emmit(self, space, source):
        #self.emmit_point = np.random.choice(np.arange(len(source.E_list)), p = source.E_weight/space.total_energy)
        #self.ix0 = space.E_list[self.emmit_point,1]
        #self.iy0 = space.E_list[self.emmit_point,0]
        count = 0
        #pprint = print_progress(len(space.E_list))  
        for n in range(len(source.E_array)):
            i = source.E_list[n]
            w = source.E_weight[n]
            count +=1
            for k in range(space.nz):
                if source.DeltaN_toDeplete[i[0],i[1],k] > 0.0:
                    #pprint.write_percent(count/len(space.stim_list))
                    source.energy = source.DeltaN_toDeplete[i[0],i[1],k]
                    P = 1-np.exp(-space.dt/space.tau[i[0],i[1]])
                    #print("p",P)
                    #print(source.energy / source.photon_energy)
                    source.energy = source.energy * source.quantumyield * source.E_conv * P
                    source.energy = source.photon_energy * np.floor(source.energy/source.photon_energy)
                    #print("\# of photons Spont", source.energy/source.photon_energy)
                    if 100*np.floor(source.energy/source.photon_energy) < self.max_n_photons:
                        self.n_photons = int(100*np.floor(source.energy/source.photon_energy))
                    else:
                        self.n_photons = self.max_n_photons
                    self.n_photons = self.max_n_photons
                    source.energy *= self.n_photons
                    if source.energy > 0:
                        #print("energy",source.energy)
                        source.spont_x = space.xlin[i[1]]
                        source.spont_y = space.ylin[i[0]]
                        source.spont_z = space.zlin[k]
                        source.DeltaN_toDeplete[i[0],i[1],k] -= source.energy/self.n_photons
                        for p in range( self.n_photons):
                            #print(p)
                            self.loop_photon(space, source, p)
                        self.photon_count += self.n_photons
        #pprint.clear()
    def stimulated_emmit(self, space, source): 
        #pprint = print_progress(len(space.stim_list))
        for i in range(len(space.stim_list)):  
            #print(i)
            #print(space.stim_array)
            ix = space.stim_array[i].idx
            iy = space.stim_array[i].idy
            iz = space.stim_array[i].idz
            if space.DeltaN_toDeplete[iy,ix,iz] > 0.0:
                #pprint.write_percent(i/len(space.stim_list))
                space.energy = space.DeltaN_toDeplete[iy,ix,iz]
                w21 = c**2 * space.Iv / (8*pi*space.n[iy,iy]**2 * h * (c / space.stim_array[i].wavelength) * space.tau[iy,ix])
                print(w21)
                P = 1 - np.exp(-space.dt*w21)
                print("p",P)
                source.energy = source.energy * source.quantumyield * source.E_conv * P
                #print(source.energy / source.photon_energy)
                #print("\# of photons Stim", source.energy/source.photon_energy)
                if np.floor(source.energy/source.photon_energy) < self.max_n_photons:
                    self.n_photons = int(np.floor(source.energy/source.photon_energy))
                    source.energy =   source.photon_energy
                else:
                    self.n_photons = self.max_n_photons
                self.n_photons = self.max_n_photons
                source.energy *= self.n_photons
                if source.energy > 0:
                    #print("energy",source.energy)
                    source.stim_x = space.xlin[ix]
                    source.stim_y = space.ylin[iy]
                    source.stim_z = space.zlin[iz]
                    source.stim_phi = space.stim_array[i].phi
                    source.stim_theta = space.stim_array[i].theta
                    space.DeltaN_toDeplete[iy,ix,iz] -= source.energy/self.n_photons
                    for p in range( self.n_photons):
                        self.loop_photon(space, source, p)
                    self.photon_count += self.n_photons
        print("sum stim", sum(space.stim_energy))
        #pprint.clear()
