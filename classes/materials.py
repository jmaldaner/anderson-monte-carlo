__author__ = "James Maldaner"

from classes.functions.constants import *

# Types of Materials used in the Fibres

class Material(object):
    #http://pages.mtu.edu/~scarn/teaching/GE4250/transmission_lecture.pdf
    def __init__(self , n_pump = 1, n_emmit = 1, mua_pump = 1, mua_emmit = 1, mus_pump = 1, mus_emmit = 1, g = 0.99, tau = 10**10, wavelength_absorb = 532 * nano, wavelength_emmit = 580 * nano, quantumyield = 0.31):
        self.n_pump = n_pump #Index of Refraction
        self.n_emmit = n_emmit #Index of Refraction
        self.mua_pump = mua_pump # Absorption Coefficient [m^-1]
        self.mua_emmit = mua_emmit # Absorption Coefficient [m^-1]
        self.mus_pump = mus_pump #Scattering Coefficient [m^-1]
        self.mus_emmit = mus_emmit #Scattering Coefficient [m^-1]
        self.g = g # Anisotropy [-]
        self.mue_pump = self.mus_pump + self.mua_pump
        self.mue_emmit = self.mus_emmit + self.mua_emmit        
        self.tau = tau
        self.wavelength_emmit = wavelength_emmit
        self.wavelength_absorb = wavelength_absorb
        self.quantumyield = quantumyield

        
class Ethanol(Material):
    def __init__(self):
        # http://www.icmp.lviv.ua/journal/zbirnyk.46/012/art12.pdf
        #https://refractiveindex.info/?shelf=organic&book=ethanol&page=Sani
        n_pump = 1.37
        n_emmit = 1.37
        mua_pump = 6.8 * 10**(-2)
        mua_emmit = 6.8 * 10**(-2)
        mus_pump = 0 
        mus_emmit = 0        
        g = 0.8 #?
        self.name = "Ethanol"
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)
        
class Benzene(Material):
    def __init__(self):
        # http://www.icmp.lviv.ua/journal/zbirnyk.46/012/art12.pdf
        n_pump = 1
        n_emmit = 1
        mua_pump = 4.0 * 10**(-2)
        mua_emmit = 4.0 * 10**(-2)
        mus_pump = 0
        mus_emmit = 0
        g = 0.99 #?
        self.name = "Benzene"
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)

class Silica(Material):
    def __init__(self):
        #https://www.crystran.co.uk/optical-materials/silica-glass-sio2
        #https://refractiveindex.info/?shelf=main&book=SiO2&page=Gao
        n_pump = 1.46
        n_emmit = 1.46
        mua_pump = 10*10**(-4) /centi
        mua_emmit = 10*10**(-4) /centi
        mus_pump = 0 #?
        mus_emmit = 0 #?
        g = 0 #?
        self.name = "Silica"
        

        
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)

class Air(Material):
    def __init__(self):
        n_pump = 1
        n_emmit = 1
        mua_pump = 0
        mua_emmit = 0
        mus_pump = 0
        mus_emmit = 0
        g = 1
        self.name = "Air"
        
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)

class TestFibreCore(Material):
    def __init__(self):
        n_pump = 4
        n_emmit = 4
        mua_pump = 0
        mua_emmit = 0
        mus_pump =0.00000001
        mus_emmit =0.00000001
        g = 0.9
        
        self.name = "Test Core"
        
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)

class TestFibreClad(Material):
    def __init__(self):
        n_pump = 2
        n_emmit = 2
        mua_pump = 0
        mua_emmit = 0
        mus_pump =0.00000001
        mus_emmit =0.00000001
        g = 0.9
        self.name = "Test Clad"
        
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g)
        
class Rhodamine640Ethanol(Material):
    # Spectral and temporal measurements of laser action
    # of Rhodamine 640 dye in strongly scattering media
    
    # Experimental recovery of absorption, scattering and fluorescence parameters in highlyscattering
    # media from a single frequency measurement
    
    #http://www.iss.com/resources/reference/data_tables/LifetimeDataFluorophores.html 
    def __init__(self, Ethanol):

        
        n_pump = Ethanol.n_pump
        n_emmit = Ethanol.n_emmit
        mua_pump = 0.124 / centi
        mua_emmit = 0.076 / centi
        mus_pump = 10.9/centi
        mus_emmit = 10.9/centi
        g = 0.8 #?
        tau = 1.68 * nano
        self.name = Ethanol.name
        self.name += " + Rhodamine 640"
        
        Material.__init__(self,n_pump, n_emmit,mua_pump, mua_emmit, mus_pump, mus_emmit,g,tau)
