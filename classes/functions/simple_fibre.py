import numpy as np
def simple_fibre(r_max, r_inner, array_size, n_core=0, n_clad=1, n_outter=2):
    fibre = np.zeros((array_size+1, array_size+1))
    x = np.linspace(-r_max, r_max, int(array_size)+1)
    y = np.linspace(-r_max, r_max, int(array_size)+1)
    for i in range(len(x)):
        for j in range(len(y)):
            r2 = (x[i]**2 + y[j]**2)
            if r2 > r_max**2:
                fibre[j,i] = n_outter
            elif r2 > r_inner**2:
                fibre[j,i] = n_clad
            else:
                fibre[j,i] = n_core
                

    return(fibre)