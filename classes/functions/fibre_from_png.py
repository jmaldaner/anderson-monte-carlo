import numpy as np
from scipy import misc
import glob
def fibre_from_png(r_max, image_path, n_core=0, n_clad=1, n_outter=2):
    image = misc.imread(image_path)
    array_size = len(image) # Must be odd
    
    fibre = np.zeros((array_size, array_size))
    x = np.linspace(-r_max, r_max, int(array_size))
    y = np.linspace(-r_max, r_max, int(array_size))


    
                
    for i in range(array_size):
        for j in range(array_size):
            if image[j,i,0] > 100: #white or red
                if image[j,i,2] > 100: # while
                    fibre[j,i] = n_clad
                else:
                    fibre[j,i] = n_outter
            else: # Black
                fibre[j,i] = n_core
                


    return(fibre)