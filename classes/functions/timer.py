import time 
class timer(object):
    def __init__(self):
        self.start = time.time()
        
    def report(self):
        self.time_raw = time.time() - self.start
        self.total_time = time.strftime('%H:%M:%S', time.gmtime(time.time() - self.start))
