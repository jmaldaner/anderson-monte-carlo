import os
import time
import numpy as np
import pickle
def pickle_variables(space, show = False):
    print("-"*100)
    back = '../data/'
    s = '/'
    d = '-'
    c = 1
    month = time.strftime('%B')
    day = time.strftime('%d')
    hour = time.strftime('%H')
    min = time.strftime('%M')
    sec = time.strftime('%S')

    folder = back + month + s + day + s + hour + d + min
    if not os.path.exists(back + month):
        os.makedirs(back + month)
    if not os.path.exists(back + month + s + day):
        os.makedirs(back + month + s + day)
    if not os.path.exists(folder):
        os.makedirs(folder)
        print('Created the directory: '+ folder)
    else:
        folder = folder + d + sec
        os.makedirs(folder)
        print('Created the directory: '+ folder)
        
    print("Saving Data")
    # Save Arrays        
    np.save( folder + s + "zlin", space.zlin)
    np.save( folder + s + "xlin", space.xlin)
    np.save( folder + s + "ylin", space.ylin)
    space_dict = open( folder + s + "space_dict.pickle", "wb")


    
    pickle.dump(space.sources_dict, space_dict)
    space_dict.close()
    for key, value in space.sources_dict.items():
        np.save( folder + s + key + "_absorbed",  space.pp[key].absorbed )
        np.save( folder + s + key + "_End-Intensity", space.pp[key].end_intensity)
        np.save( folder + s + key + "_Front-Intensity", space.pp[key].front_intensity)
        np.save( folder + s + key + "_Side-Intensity",  space.pp[key].side_intensity)
        np.save( folder + s + key + "_Source-Intensity",  space.pp[key].source_intensity)
        
    if "Spontaneous" in space.sources_dict:
        np.save( folder + s +  "All_Spontaneous_absorbed",  space.spont_absorbed )
        print(sum(sum(space.spont_end_intensity)))
        np.save( folder + s +  "All_Spontaneous_End-Intensity", space.spont_end_intensity)
        np.save( folder + s +  "All_Spontaneous_Front-Intensity", space.spont_front_intensity)
        np.save( folder + s +  "All_Spontaneous_Side-Intensity",  space.spont_side_intensity)
    if "Stimulated" in space.sources_dict:
        np.save( folder + s +  "All_Stimulated_absorbed",  space.stim_absorbed )
        np.save( folder + s +  "All_Stimulated_End-Intensity", space.stim_end_intensity)
        np.save( folder + s +  "All_Stimulated_Front-Intensity", space.stim_front_intensity)
        np.save( folder + s +  "All_Stimulated_Side-Intensity",  space.stim_side_intensity)    
    np.save( folder + s + "Material-Type", space.m)
    np.save( folder + s + "DeltaN", space.DeltaN )
    np.save( folder + s + "E_array", space.E_array)
    np.save( folder + s + "E_list", space.E_list)
    np.save( folder + s + "E_weight", space.E_weight)
    
    print("Saving Metadata")
    #Creat Metadata string
    text_file = open(folder  + "/meta.txt", "w")
    mat_type= ["Core", "Clad", "Surround"]
    if len(mat_type) < len(space.materials):
        mat_type += ""
    
    file_string = ("Space Data\n" + 
                    "R max[m]: {:.2E}\n".format(space.r_max)+
                    "R size: {}\n".format(space.nx) +
                    "R Division[m]: {}\n".format(space.dx) + 
                    "Length of Caviy[m]: {:.2E}\n".format(space.length_of_cavity)+
                    "Z size: {}\n".format(space.nz)+
                    "Z Division[m]: {:.2E}\n".format(space.dz)+
                    "\n")
    for key, value in space.materials.items():


        file_string += ("Material: {} \n".format(key) +
                       "Type: {}\n".format(value.name)+
                       "Index of Refraction Pump: {}\n".format(value.n_pump) +
                       "Absorption Coefficient Pump [m^-1]: {}\n".format(value.mua_pump) +
                       "Scattering Coefficient Pump [m^-1]: {}\n".format(value.mus_pump) +
                       "Index of Refraction Emmit: {}\n".format(value.n_emmit) +
                       "Absorption Coefficient Emmit [m^-1]: {}\n".format(value.mua_emmit) +
                       "Scattering Coefficient Emmit [m^-1]: {}\n".format(value.mus_emmit) +
                       "Anisotropy: {}\n".format(value.g) +
                       "Tau 21 [s]: {}\n".format(value.tau) +
                       "\n")
    for key, value in space.sources_dict.items() :
        np.savetxt( folder + s +key + "Photons.csv", space.pp[key].photon_data,  delimiter=",")
        if value.type == 'Pump':
            file_string += (key+" \n" +
                           "x [m]: {:.2E}\n".format(value.x0) +
                           "y [m]: {:.2E}\n".format(value.y0) + 
                           "z [m]: {:.2E}\n".format(value.z0)+
                           "Ange Spread [deg]: {:.2E}\n".format(value.phi) +
                           "Spot Size [m]: {:.2E}\n".format(value.dr) +
                           "\n" +
                           "Photon Pack Data\n" +
                           "N Photons: {}\n".format(space.pp[key].n_photons) +
                           "Total Energy[J]: {}\n".format(np.sum(np.sum(space.pp[key].end_intensity))+
                                                            np.sum(np.sum(space.pp[key].side_intensity))+
                                                            np.sum(np.sum(space.pp[key].front_intensity))+
                                                            np.sum(space.pp[key].absorbed)))
        if value.type == 'Spontaneous':
            file_string += (key+"\n" +
                            "wavelength [m]: {:.2E}\n".format(value.wavelength)+
                            "Photon Energy [J]: {:.2E}\n".format(value.photon_energy)+
                            "\n"+
                           "Photon Pack Data\n" +
                           "N Photons: {}\n".format(space.pp[key].n_photons)  +
                           "Total Energy[J]: {}\n".format(np.sum(np.sum(space.pp[key].end_intensity))+
                                                            np.sum(np.sum(space.pp[key].side_intensity))+
                                                            np.sum(np.sum(space.pp[key].front_intensity))+
                                                            np.sum(space.pp[key].absorbed)))
        if value.type == 'Stimulated':
            file_string += (key+"\n" +
                            "wavelength [m]: {:.2E}\n".format(value.wavelength)+
                            "Photon Energy [J]: {:.2E}\n".format(value.photon_energy)+
                            "\n"+
                           "Photon Pack Data\n" +
                           "N Photons: {}\n".format(space.pp[key].n_photons)  +
                           "Total Energy[J]: {}\n".format(np.sum(np.sum(space.pp[key].end_intensity))+
                                                            np.sum(np.sum(space.pp[key].side_intensity))+
                                                            np.sum(np.sum(space.pp[key].front_intensity))+
                                                            np.sum(space.pp[key].absorbed)))
        try: space.pp[key].imported
        except AttributeError:
            for i in range(len(space.pp[key].unique)):
                if space.pp[key].unique[i] == 1:
                   file_string +="Percent Exit End: {:.4}%\n".format(100 * space.pp[key].counts[i]/space.pp[key].n_photons)
                elif space.pp[key].unique[i] == 2:
                   file_string +="Percent Exit Side: {:.4}%\n".format(100 * space.pp[key].counts[i]/space.pp[key].n_photons)
                elif space.pp[key].unique[i] == 3:
                   file_string +="Percent Absorbed: {:.4}%\n".format(100 * space.pp[key].counts[i]/space.pp[key].n_photons) 
                elif space.pp[key].unique[i] == 4:
                   file_string +="Percent Exit Front: {:.4}%\n".format(100 * space.pp[key].counts[i]/space.pp[key].n_photons) 
        
        file_string += "\n"
        
        
    file_string +="Time to run Simulation (HH-MM-SS): {}\n".format(space.clock.total_time)
                   

    text_file.write(file_string)
    text_file.close()
    if show:
        print(file_string)
    return folder
    
    
    
    
