http://rogerdudler.github.io/git-guide/

# Anderson Localization Simulation using Monte Carlo Photon Propeation

This project seaks to simulate Anderson Localization in 2D disordered fibres.

## Getting Started


### Prerequisites

* numpy
* matplotlib
* Scipy

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With



## Contributing


## Versioning


## Authors

* **James Maldaner** - *Initial work* 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License



## Acknowledgments


