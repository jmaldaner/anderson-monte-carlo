import pickle
from classes.functions.constants import *
from classes.functions import simple_fibre
from classes.functions import fibre_from_png
from classes import materials

class store(object):
    def __init__(self):
        pass

path = 'classes/config/'
title = 'space-rmax125-smin02-1000-l001-nz100'
data = store()
#########################
# If saving space data  #
#########################
# space-rmax250-smin15-100

if True:
    data.nx = 1001
    data.ny = 1001
    data.nz = 100
    data.length_of_cavity = 0.001
    data.r_max = 125  * micro
    data.s_min = 0.2 * micro

##########################
# If saving pump data    #
##########################
# pump-origin-8umdr-01deg

if False:
    data.x0 = 0
    data.y0 = 0
    data.dphi = 0.001 * 2 * pi / 360
    data.dr = 8 * micro
    data.z = 0
    
###########################
# If saving fibre profile #
###########################
# fibre-profile-simple-1000

if False:
    data.m1 = fibre_from_png.fibre_from_png(250 * micro, "../viewmedia.png")
    print(data.m1[0,0])
    #data.m1 = simple_fibre.simple_fibre(250 * micro, 15 * micro, 10)

##############################
# If saving material profile #
##############################
# materials-test-fibre

if False:
    n = materials.Ethanol()
    data.n0 = materials.Rhodamine640Ethanol(n) # Core
    data.n1 = materials.Silica() # Clad
    data.n2 = materials.Air()           # Surround

file_name = path + title + '.pkl'
output = open(file_name, 'wb')    
pickle.dump(data, output)
output.close()
print("Created the File ",file_name)
