__author__ = "James Maldaner"


from classes.functions.constants import *
from classes.functions.timer import timer
from classes.functions.pickle_variables import pickle_variables
from classes.functions.unpickle_variables import unpickle_variables
from classes.photonpacket import PhotonPacket
import pickle
from classes.functions.print_progress import print_progress
import numpy as np
import csv

random = np.random.uniform
class Space(object):

    def __init__(self, nx = 10, ny = 10, nz = 10, length_of_cavity = 0.1, r_max = 250 * micro, s_min = 15 * micro, data = None):
        self.clock= timer()
        print("\n"*5)
        print("#"*100)
        print("# Starting Simulation ")
        print("#"*100)
        if data == None:
            self.length_of_cavity = length_of_cavity 
            self.r_max = r_max
            self.nx = nx # Number of divisions in R
            self.ny = ny # Number of division in theta
            self.nz = nz # Number of divisions in z
        else:
            self.length_of_cavity = data.length_of_cavity
            self.r_max = data.r_max
            self.nx = data.nx
            self.ny = data.ny
            self.nz = data.nz
            
        self.threshold = 0.001
        self.chance = 0.1
        self.max_count = 0
        self.energy_threshold = 100000
        
        
        
        self.pp = {}
        self.sources_dict = {}
        self.materials = {}
        self.source_names = []

        
        
        #Create ranges for spatial coordinates
        self.xlin, self.dx = np.linspace(-self.r_max,self.r_max,self.nx, retstep = True)
        self.ylin, self.dy = np.linspace(-self.r_max,self.r_max,self.ny, retstep = True)
        self.zlin, self.dz = np.linspace(0,self.length_of_cavity, self.nz, retstep = True)
        self.dr = r_max/nx
        self.s_min = s_min 
        #f self.dz > self.s_min:
        #    self.s_min = self.dz
        
        #Create mesh grids for spatial coordinates
        self.x, self.y, self.z = np.meshgrid(self.xlin, self.ylin, self.zlin)
        
        # Inversion
        self.AbsE = {}#np.zeros(np.shape(self.x))#,dtype = np.float64)
    def __str__(self):
        print('x',[-r_max, r_max])
        print('y',[-r_max, r_max])
        print('z',[0,len_cavity])
    
    def __getattr__(self, key):
        return False
        
    def material_creation(self,m, mat):
        '''m is an array of 1,0 where every point that has a 1 is mat_1 and every point that has a 0 is mat_0
        Could be expanded to include other material properties
        Is assuming that the fibre is similar all the way down
        '''
        self.m = m
        print("-"*100)
        print("Creating material arrays")
        self.space_size = np.ones(m.shape)
        self.n_pump = np.ones(m.shape)
        self.n_emmit = np.ones(m.shape)
        self.mua_pump = np.ones(m.shape)
        self.mua_emmit = np.ones(m.shape)
        self.mus_pump = np.ones(m.shape)
        self.mus_emmit = np.ones(m.shape)
        self.g = np.ones(m.shape)
        self.mue_pump = np.ones(m.shape)
        self.mue_emmit = np.ones(m.shape)
        self.tau = np.ones(m.shape)
        for i in range(len(m)):
            for j in range(len(m)):
                for k in range(len(mat)):
                    if m[j,i] == k:
                        self.n_pump[j,i] = mat[k].n_pump
                        self.n_emmit[j,i] = mat[k].n_emmit
                        self.mua_pump[j,i] = mat[k].mua_pump
                        self.mua_emmit[j,i] = mat[k].mua_emmit
                        self.mus_pump[j,i] = mat[k].mus_pump
                        self.mus_emmit[j,i] = mat[k].mus_emmit
                        self.g[j,i] = mat[k].g
                        self.mue_pump[j,i] = mat[k].mue_pump
                        self.mue_emmit[j,i] = mat[k].mue_emmit
                        self.tau[j,i] = mat[k].tau
        self.material_names = []               
        for i in  range(len(mat)):
            self.materials[mat[i].name] = mat[i]
            self.material_names.append(mat[i].name)
    
    def pump_space(self, source, n_photons):
        print("-"*100)
        print("Pumping space: Initiated with {}J".format(source.energy))
        self.initiate_source(source)
        
        self.n = self.n_pump
        self.mua = self.mua_pump
        self.mus = self.mus_pump
        self.mue = self.mue_pump
        
        self.pp[source.name] = PhotonPacket(n_photons)
        self.pp[source.name].launch_pump_photons(self, source)
        
        self.terminate_source(source)
        
        print("Pumping space complete", " "*11)
        self.energy_analysis() 
    
    def store_load(self):
        folder = pickle_variables(self)
        load = unpickle_variables(folder)
        load.plot_data()
        return load
    def initiate_source(self, source):

        if not self.source_num: self.source_num = {}
        source.source_intensity = np.zeros(np.shape(self.space_size))
        if source.type not in self.sources_dict:
            name = source.type
            self.source_num[source.type] = 1
        else:
            self.source_num[source.type] += 1
            name = source.type + str(self.source_num[source.type]).zfill(4)

        self.sources_dict[name] = source
        #print(name)
        #print(self.sources_dict[name],source)
        self.source_names.append(name)
        source.name = name
        
        if source.type == "Pump":
            self.n = self.n_pump
            self.mua = self.mua_pump
            self.mus = self.mus_pump
            self.mue = self.mue_pump
            self.DeltaN = np.zeros(np.shape(self.x))
            print("pump")
        else :
            self.n = self.n_emmit
            self.mua = self.mua_emmit
            self.mus = self.mus_emmit
            self.mue = self.mue_emmit
            source.E_list = self.E_list
            source.E_weight = self.E_weight
            source.E_array = self.E_array
            source.DeltaN = self.DeltaN
            source.DeltaN_toDeplete = self.DeltaN_toDeplete

        return name

    def terminate_source(self, source):
        self.AbsE[source.name] = self.DeltaN
        if source.type == "Spontaneous":
            self.stim_list = self.stim_list[1:]
        
    def import_pump(self, file_loc, mult):
        self.n = self.n_pump
        self.mua = self.mua_pump
        self.mus = self.mus_pump
        self.mue = self.mue_pump
        self.load_sources_file = open(file_loc + "/space_dict.pickle", "rb")
        self.load_sources = pickle.load(self.load_sources_file)
        self.load_sources_file.close()
        source = self.load_sources["Pump"]
        self.sources_dict["Pump"] = self.load_sources["Pump"]
        self.source_names.append("Pump")
        fileObject = csv.reader(file_loc + "/PumpPhotons.csv")
        n_photons = sum(1 for row in fileObject)
        date_file = "/December-03-17-38-"
        
        self.pp[source.name] = PhotonPacket(n_photons)
        self.pp[source.name].imported = True
        self.pp[source.name].absorbed = np.load(file_loc  +"/Pump_absorbed.npy")
        self.pp[source.name].end_intensity = np.load(file_loc +"/Pump_End-Intensity.npy")
        self.pp[source.name].side_intensity = np.load(file_loc  +"/Pump_Side-Intensity.npy")
        self.pp[source.name].front_intensity  = np.load(file_loc  +"/Pump_Front-Intensity.npy")
        self.pp[source.name].source_intensity  = np.load(file_loc  +"/Pump_Source-Intensity.npy")
        self.DeltaN = np.load(file_loc + "/DeltaN.npy") * mult
        try:
            self.E_array = np.load(file_loc + "/E_array.npy")
            self.E_list = np.load(file_loc + "/E_list.npy")
            self.E_weight = np.load(file_loc + "/E_weight.npy")
        except FileNotFoundError:
            print("Energy arrays do not exist, creating them now")
            self.energy_analysis()
    def energy_analysis(self):
        self.E_array = np.zeros(self.n.shape) # Energy at each x,y
        self.E_list = [0,0] # List of points that have energy
        self.E_weight = [0] # Weight at each point in E_list
        p = print_progress(self.nx * self.ny, 'energy')
        print("-"*100)
        print("Starting Energy Analysis")
        for j in range(self.ny):
            for i in range(self.nx):
                self.E_array[j,i] = sum(self.DeltaN[j,i])
                if self.E_array[j,i] > 0.0:
                    self.E_list = np.vstack([self.E_list,[j,i]])
                    self.E_weight = np.append(self.E_weight,self.E_array[j,i])
                p.write_percent((j * self.ny + i)/(self.nx * self.ny) )   
        p.clear()  
        self.E_list = self.E_list[1:]
        self.E_weight = self.E_weight[1:]
        print("Stored Energy: {:.2E}".format(sum(self.E_weight)))
        print("Energy Analysis Complete")
          
    def initiate_emission(self, spont_source, stim_source, n_photons): 
        print("-"*100)
        print("Emission: Started")
        self.total_energy = sum(self.E_weight)
        self.tau_min = np.amin(self.tau)
        self.t_max = - np.log(0.1) * self.tau_min
        self.DeltaN_toDeplete = self.DeltaN
        self.DeltaN = np.zeros(np.shape(self.x))
        self.t = 0
        
        self.Iv = 0
        self.stim_array = []
        self.stim_list = [0,0,0] # List of points that have energy
        self.stim_energy = [0]
        if self.total_energy != 0:
            
           while self.t < self.t_max:
               print("Percent Left : {}".format(self.t / self.t_max * 100))
               self.emission_loop(spont_source, stim_source, n_photons)
              
            
        print("Emission completed",' '*15)

    def time_hop(self):
        P = 1-random() / 2
        self.dt = -np.log(P) * self.tau_min
        self.t += self.dt
        return P
    
    def emission_loop(self,spont_source, stim_source, n_photons):
        
        P = self.time_hop()

        
        self.spont_end_intensity =  np.zeros(np.shape(self.n))
        self.spont_side_intensity = np.zeros(np.shape(self.n))
        self.spont_absorbed = np.zeros(np.shape(self.zlin))
        self.spont_front_intensity = np.zeros(np.shape(self.n))
        
        self.stim_end_intensity =  np.zeros(np.shape(self.n))
        self.stim_side_intensity = np.zeros(np.shape(self.n))
        self.stim_absorbed = np.zeros(np.shape(self.zlin))
        self.stim_front_intensity = np.zeros(np.shape(self.n))

        if self.Iv != 0:self.stim_emission( stim_source, n_photons)
        
        self.spont_emission( spont_source, n_photons)
       
        
        
        

    def spont_emission(self, source, n_photons):
        print("Spont Emission") 
        self.initiate_source(source)

        self.pp[source.name] = PhotonPacket(n_photons)#int(n_photons / len(self.E_array)))
        self.pp[source.name].launch_spont_photons(self, source)
        self.terminate_source(source)
        if self.pp[source.name].n_photons == 0: 
            self.pp.pop(source.name,None)  
            self.sources_dict.pop(source.name, None)
        else:
            #print(sum(sum(self.pp[source.name].end_intensity)))
            self.spont_end_intensity = np.add(self.spont_end_intensity, self.pp[source.name].end_intensity)
            self.spont_side_intensity = np.add(self.spont_side_intensity, self.pp[source.name].side_intensity)
            self.spont_absorbed = np.add(self.spont_absorbed, self.pp[source.name].absorbed)
            self.spont_front_intensity = np.add(self.spont_front_intensity, self.pp[source.name].front_intensity)

        #if (int(n_photons / len(self.E_array)) != 0):
        self.Iv = sum(self.stim_energy)#/int(n_photons / len(self.E_array))
        print(self.Iv, "Iv")
        #print(sum(self.stim_energy))
        pass
            
    def stim_emission(self, source, n_photons):
        print("Stim Emission")

        self.initiate_source(source)
        

        
        self.pp[source.name] = PhotonPacket(n_photons )#int(n_photons / len(self.stim_list)))
        self.pp[source.name].launch_stim_photons(self, source)
 
        self.terminate_source(source)
        if self.pp[source.name].n_photons == 0: 
            self.pp.pop(source.name,None)  
            self.sources_dict.pop(source.name, None)
        else:
            
            print(sum(sum(self.pp[source.name].end_intensity)))
            self.stim_end_intensity = np.add(self.stim_end_intensity, self.pp[source.name].end_intensity)
            self.stim_side_intensity = np.add(self.stim_side_intensity, self.pp[source.name].side_intensity)
            self.stim_absorbed = np.add(self.stim_absorbed, self.pp[source.name].absorbed)
            self.stim_front_intensity = np.add(self.stim_front_intensity, self.pp[source.name].front_intensity)
        pass
