import time
import sys
import numpy as np

class print_progress(object):
    def __init__(self, total, anal_type = "photon", step = 100):
        self.l = 3
        self.m=int(100/self.l)
        self.start = time.time()
        self.count_max = np.ceil(total/step)
        self.count = self.count_max -1
        if anal_type == "photon":
            print("Photon packet with {} total photons".format(total))
        if anal_type == "energy":
            print("Energy with {} total points".format(total))
        
    def write_percent(self,percent):
        if self.count == self.count_max:
            p = int(percent*100/3)
            if percent == 0:
                expect = 10000000
            else:
                expect = (time.time() - self.start)*(1/percent)
            elapsed = time.time()-self.start
            t = time.strftime('%H:%M:%S', time.gmtime(expect -  elapsed))
            e = time.strftime('%H:%M:%S', time.gmtime( elapsed))
            sys.stdout.write("Progress {:2.1%} Time left: {} Time elapsed {} {}\r".format(percent,t,e, chr(9608)*p + chr(9618)*(self.m-p)))
            self.count = 0
        self.count += 1
        #print(self.count)
    def clear(self):
        elapsed = time.time()-self.start
        
        e = time.strftime('%H:%M:%S', time.gmtime( elapsed))
        sys.stdout.write("{}\r\r".format(" "*100))
        sys.stdout.write("\r{}\r".format(" "*100))
        sys.stdout.write("Complete after {}".format(e))
        pass
