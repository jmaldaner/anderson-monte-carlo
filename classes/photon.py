__author__ = "James Maldaner"

import numpy as np
import math
from classes.functions.constants import *
from classes.functions.find_nearest import find_nearest

random = np.random.uniform
gauss = np.random.normal


class Photon(object):
    def __init__(self,space, source, photon_num = 1, diagnose = False):
        self.photon_num = photon_num
        self.status = True
        self.max_w = source.energy
        self.w = source.energy  #Initial weight
        self.count = 0
        self.diagnose = diagnose
        self.x, self.y,self.z, self.phi, self.theta = source.launch()

        source.source_intensity = np.zeros(np.shape(space.n))
        self.wavelength = source.wavelength
        self.type = source.type
        if abs(self.phi) > 0.1: 
            self.status = False
            #print("stim fail", self.ph)
        else:
            if self.type == "Stimulated":
                print("stim")
        self.sinphi = np.sin(self.phi) 
        self.cosphi = np.cos(self.phi)
        self.sintheta = np.sin(self.theta)
        self.costheta = np.cos(self.theta)
        self.ux =  self.sinphi * self.costheta
        self.uy =  self.sinphi * self.sintheta
        self.uz = self.cosphi
        if self.diagnose:
            print("="*50)
            print("="*50)
            print("Number {}".format(self.photon_num))
            print("Initial Direction (x,y,z)")
            print(self.ux, self.uy, self.uz)
            print("Initial Position (x,y,z)")
            print(self.x, self.y, self.z)
            
        self.closest_xy( space)
        source.source_intensity[self.idy, self.idx] += 1
        self.n = space.n[self.idy,self.idx]
            
    def __str__(self):
        string =[self.x, self.y,self.z, self.phi, self.theta]
        return 'x,y,z,phi,theta: '+str(string)
    
    def __getattr__(self, key):
        return False
    
    def closest_xy(self, space):
        if True:#self.idx == False:
            self.idx = find_nearest(space.xlin, self.x)
            self.dx = space.xlin[self.idx]
            

            self.idy = find_nearest(space.ylin, self.y)
            self.dy = space.ylin[self.idy]
        else:
            if self.sinphi ==0.0:
                pass
            else:
                dx = abs(self.ux * self.sinphi)  #Find the max x distance traveled
                dy = abs(self.uy * self.sinphi)  #Find the max y distance travled
                deltaidx =  math.ceil(dx / space.dx) # Find the max x index change
                deltaidy =  math.ceil(dy / space.dy) # Find the max y  index change
                
                if ((self.idx + deltaidx) >  space.nx) or ((self.idx - deltaidx) < 0):
                    self.idx = find_nearest(space.xlin, self.x)
                    self.dx = space.xlin[self.idx]
                else:

                    small_x = space.xlin[(self.idx - deltaidx-1):(self.idx + deltaidx +1)]
                    
                    if len(small_x)< 1:
                        pass
                    else:
                        self.idx = find_nearest(small_x, self.x_new) + self.idx 
                        #print(self.idx)
                        if self.idx >= len(space.xlin):
                            self.idx = len(space.xlin)-1
                        self.dx = space.xlin[self.idx]


                if ((self.idy + deltaidy) >  space.ny) or ((self.idy - deltaidy) < 0):
                    self.idy = find_nearest(space.ylin, self.y)
                    self.dy = space.ylin[self.idy]
                else:

                    small_y = space.ylin[(self.idy - deltaidy-1):(self.idy + deltaidy+1)]
                    if len(small_y) < 1:
                        pass
                    else:
                        self.idy = find_nearest(small_y, self.y_new) + self.idy
                        if self.idy >= len(space.ylin):
                            self.idy = len(space.ylin)-1
                        self.dy = space.ylin[self.idy]
            if self.idy == -1:
                
                self.idy = 0
            if self.idx == -1:
               
                self.idx = 0

            if self.diagnose:    
                print("Closest xy difference")
                print(self.x - self.dx, self.y -self.dy)
 
    def closest_z(self,space):
        if self.idz == False:
        #if True:
            self.idz = find_nearest(space.zlin,self.z)+1
            self.dz = space.zlin[self.idz]
        else:
            
            if self.cosphi != 0:
                
                dr = space.length_of_cavity/( space.nz) #find the distance between points (assuming x,y are the same)
                dz = self.uz * self.cosphi
                deltaidz = math.ceil(dz/dr)
                if deltaidz >  space.nz:
                
                    self.idz = find_nearest(space.zlin,self.z)+1
                    self.dz = space.zlin[self.idz]
                else:
                    small_z = space.xlin[(self.idz - deltaidz-1):(self.idz + deltaidz+1)]
                    if len(small_z) < 1:
                        pass
                    else:
                        self.idz = find_nearest(small_z, self.z_new) + self.idz
                        if self.idz >= len(space.zlin):
                            self.idz = len(space.zlin)-1
                        self.dz = space.zlin[self.idz]
        if self.idz < 0:
            print(self.idz, self.z)
        if self.diagnose:    
            print("Closest z difference")
            print(self.z - self.dz)
        
    def _hop(self, space):
        '''
        Moves the photon
        '''
        #print(self.w)
        if self.diagnose:
            print("-"*50)
            print("-"*50)
            print("New Hop")
            print(self.count)
        self.closest_xy(space)
        self.closest_z(space)
        if self.type != "Pump":
            if ([self.idy, self.idx] in space.E_list) and space.DeltaN_toDeplete[self.idy, self.idx, self.idz] !=0 :
                space.stim_array.append(self)
                space.stim_list = np.vstack([space.stim_list,[self.idy, self.idx,self.idz]])
                if space.stim_list[0,0] == space.stim_list[0,1] == space.stim_list[0,2] == 0:
                    space.stim_list = [self.idy, self.idx,self.idz]
                space.stim_energy.append(self.w)
                #print(space.stim_energy)
                

        if space.mue[self.idy, self.idx] > 0:
            if space.s_min > (1/space.mue[self.idy, self.idx]):
                self.s_min = (1/space.mue[self.idy, self.idx])
            else:
                if abs(self.phi) >0:
                    self.s_min = space.s_min / abs(self.sinphi)
                    if self.s_min > space.length_of_cavity:
                        self.s_min = space.length_of_cavity
        else:
            if abs(self.phi) >0:
                self.s_min = space.s_min / abs(self.sinphi)
                if self.s_min > space.length_of_cavity:
                    self.s_min = space.length_of_cavity
        self.s = random() * self.s_min
        
        self.x_old = self.x
        self.y_old = self.y
        self.z_old = self.z 
        
        
        self.x_new = self.x + self.s * self.ux
        self.y_new = self.y + self.s * self.uy
        self.z_new = self.z + self.s * self.uz
        self.closest_xy( space)
        
        if self.diagnose:
            print("New Coordinates")
            print(self.x_new, self.y_new, self.z_new)
            print("Weight")
            print(self.w)
            print("Jump Size")
            print(self.s)
            print("Minimum Jump Size")
            print(self.s_min)

        
        self.n_new = space.n[self.idy,self.idx]
        if self.diagnose:
            print("-"*50)
            print("Side Test")
        self._side_boundary(space,self.x_new,self.y_new)
        if self.diagnose:
            print("-"*50)
            print("End Test")
        self._end_boundary(space,self.x_new,self.y_new,self.z_new)
        
        if self.diagnose:
            print("-"*50)
            print("Interaction")
        if self.status:
            self._interaction(space)
        if (self.x_old == self.x) and (self.y_old == self.y) and (self.z_old == self.z):
            if self.diagnose:
                print("Update coordinates with no interaction")
            self.x += self.s * self.ux
            self.y += self.s * self.uy
            self.z += self.s * self.uz
        #print(self.phi, self.photon_num, self.count)
        if self.diagnose:
            print("-"*50)
            print("End of Hop")
            print("Updated Coordinates")
            print(self.x, self.y, self.z)
            print("Updated Jump")
            print(self.ux, self.uy, self.uz)
        
    def _interaction(self, space):
        rnd = random()
        self.ur = np.sqrt(self.ux**2 + self.uy**2 + self.uz**2)
        self._drop(space)
        self._spin(space)
        if space.mue[self.idy, self.idx] > 0:
            if random() < (1 - space.mus[self.idy,self.idx]/(space.mue[self.idy,self.idx])):
                self._spin(space)
        
    def _side_fresnel(self, space):
        costemp = np.sqrt(1-((self.n / self.n_new) * self.cosphi)**2)
        R_s = abs(( self.n * abs(self.sinphi)- self.n_new * costemp )/(self.n * abs(self.sinphi) + self.n_new *costemp ))**2
        R_p = abs(( self.n * costemp - self.n_new *abs(self.sinphi) )/(self.n * costemp + self.n_new * abs(self.sinphi)))**2
        R = 0.5 * (R_s + R_p)
        return R
    
    def _end_fresnel(self,space,):
        sintemp = np.sqrt(1-((self.n / self.n_new) * self.sinphi)**2)
        R_s = abs(( -self.n * sintemp + self.n_new * abs(self.cosphi))/(self.n * sintemp + self.n_new * abs(self.cosphi)))**2
        R_p = abs(( self.n *abs( self.cosphi) - self.n_new * sintemp)/(self.n * abs(self.cosphi) + self.n_new * sintemp))**2
        R = 0.5 * (R_s + R_p)
        return R
    
    def _side_refract(self, space, x,y):
        #print("\n")
        #print(self.cosphi)
        if (abs(self.n/self.n_new * self.cosphi) >= 1):
            self.phi = 0
        else:
            #print("\n")
            #print(self.phi, "old")
            #print(self.n/self.n_new * self.cosphi)
            self.phi = np.arccos(self.n/self.n_new * np.cos(self.phi)) 
            self._side_reflect(space,x,y)
            #print("Refracthgfj")
            #print(self.phi)
            #print("\n")
            if np.isnan(self.phi):
                print(self.n_new/self.n *self.cosphi)
        
        self.sinphi = np.sin(self.phi)
        self.cosphi = np.cos(self.phi)
        self.ux = self.sinphi * self.costheta
        self.uy = self.sinphi * self.sintheta
        if self.diagnose:
            print("Update Coordinates from Side Refract")
        self.x += self.s * self.ux
        self.y += self.s * self.uy
        self.z += self.s * self.uz
    
    def _side_reflect(self, space, x,y):
        self.phi =  self.phi
        self.sinphi = np.sin(self.phi)
        self.cosphi = np.cos(self.phi)
        if self.theta > pi:
            self.theta =  self.theta - pi
        else:
            self.theta = self.theta + pi
        self.sintheta = np.sin(self.theta)
        self.costheta = np.cos(self.theta)
        if self.diagnose:
            print("Update Coordinates from Side Reflect")
        self.ux = self.sinphi * self.costheta
        self.uy = self.sinphi * self.sintheta
        # Assme the change happens halfway so there is no change in x,y
        self.z += self.s * self.uz
        if self.diagnose:
            print("Bounce")
        
    def _end_reflect(self, space,z):
        if self.diagnose:
            print("Reflect")
        if z >= space.length_of_cavity:
            s = (space.length_of_cavity - z)/self.uz
        else:
            s = z / self.uz
        # Jump to End
        self.x += s * self.ux
        self.y += s * self.uy
        self.z += s * self.uz
        if self.diagnose:
            print("Jump to end")
            print(self.x, self.y, self.z)
        
        # Jump  from end
        self.uz = - self.uz
        s = 1-s/self.s
        self.x += s * self.ux
        self.y += s * self.uy
        self.z += s * self.uz
        if self.diagnose:
            print("Update Coordinates from End Refract")
        if self.diagnose:
            print("Jump from end")
            print(self.x, self.y, self.z)
        
    def _side_boundary(self, space,x,y):
        #Assuming it is hitting the new index at phi
        #https://en.wikipedia.org/wiki/Fresnel_equations

        if self.n != self.n_new:
            
            #check if internally refracted
            if (self.n_new < self.n) :
                if 1 >= abs((self.n/self.n_new) * self.cosphi):
                    self._side_refract(space,x,y)
                else:
                    self._side_reflect(space,x,y)
                    
            else:
                rnd = random()
                R = self._side_fresnel(space)
                if rnd < R: #Reflected
                    self._side_reflect(space,x,y)
                else: #refracted
                    self._side_refract(space, x,y)
  
            if np.sqrt(self.x**2 + self.y**2) > space.r_max:
                self.status = False
                self.death = 2
                if self.diagnose:
                    print("Died from Side")
                

        self.n = self.n_new
   
    def _end_boundary(self,space,x,y,z):
        if (z > space.length_of_cavity) or ( z < -0):
            R=self._end_fresnel(space)
            if R > 1: print(R)
            rnd = random()
            if self.diagnose:
                print("Reflected off end probability")
                print(self.R)
            if rnd < R:
                self._end_reflect(space,z)
                
                if self.diagnose:
                    print("Reflected jump")
                    print(self.ux, self.uy, self.uz)
                    
                
            
            else:
                self.closest_z(space)
                if z < 0:                    
                    self.death = 4
                    self.status = False
                    s = z / self.uz
                    # Jump to End
                    self.x += s * self.ux
                    self.y += s * self.uy
                    self.z += s * self.uz

                    if self.diagnose:
                        print("Update Coordinates from Side Refract")
                        print("Died from Front")

                    
                else:
                    self.death = 1
                    self.status = False
                    s = (space.length_of_cavity - z)/self.uz
                    # Jump to End
                    self.x += s * self.ux
                    self.y += s * self.uy
                    self.z += s * self.uz
                    if self.diagnose:
                        print("Update Coordinates from Side Refract")
                        print("Died from End")
                    #print("dead location")
                    #print(self.x, self.y, self.z)
                    self.closest_xy(space)
                if (self.x**2 + self.y**2) > space.r_max**2:
                    self.death = 2
                    self.status = False

                
                self.closest_xy(space)
            if self.diagnose:
                print("End Boundary final coordinates(x,y,z)")
                print(self.x, self.y, self.z)
        
    def _drop(self, space):
        rnd = random()
        self.absorb = self.w*(1-np.exp(-(space.mua[self.idy,self.idx]) * self.s))
        self.closest_z(space)
        if self.absorb < 0:
            print("absorb",self.absorb)
        if self.absorb > self.w:
            self.absorb = self.w
        else:
            self.w -= self.absorb
        space.DeltaN[self.idy, self.idx, self.idz] +=self.absorb
        #print(self.idz)
        
        if self.diagnose:
            print("Absorbe")
            print(self.absorbe)
            print("Weight Left")
            print(self.w)
    
    def _spin(self, space):
        #Get new phi
        self.g = space.g[self.idy, self.idx]
        if self.g == 0:
            rnd = random()
            self.phi = rnd*2*pi
        else:
            rnd = random()
            temp = (1.0 - self.g **2)/(1.0 - self.g * rnd)
            if (abs(1.0+ self.g**2 - temp **2)/(2.0 * self.g)) > 1:
                print('spin')
                print((1.0+ self.g**2 - temp **2)/(2.0 * self.g))
                self.phi = 0
            else:
                self.phi = np.arccos((1.0+ self.g**2 - temp **2)/(2.0 * self.g))
        
        
        # Get new theta
        rnd = random()
        self.theta = 2.0 * pi * rnd
        
        # Get new projection
        
        self.sinphi = np.sin(self.phi)
        self.cosphi = np.cos(self.phi)
        self.sintheta = np.sin(self.theta)
        self.costheta = np.cos(self.theta)
        
        # assuming close to perpendicular
        
        uxx = self.sinphi * self.costheta
        uyy = self.sinphi * self.sintheta
        uzz = self.cosphi
        
        # Update Trajectory
        
        self.ux = uxx
        self.uy = uyy
        self.uz = uzz
        
        if self.diagnose:
            print("Spin new jump (x,y,z)")
            print(self.ux, self.uy, self.uz)
               
    def _check_roulette(self, space):
        rnd = random()
        if self.w < space.threshold *self.max_w:
            if rnd < space.chance:
                pass
                self.w/=space.chance
                #if self.w < 0:
                #    self.w = space.threshold/space.chance
            else:
                self.status = False
                self.death = 3
                #self.closest_z(space)
                #space.DeltaN[self.idy, self.idx, self.idz] += self.w 
                if self.diagnose:
                    print("Died from Inside")
        
    def _loop_photon(self, space):
        '''
        Loop of Tree
        '''
        while self.status:
            self.count +=1
            self._hop(space)
            self._check_roulette(space)
            if self.count > 100000:
                self.diagnose = True
        self.diagnose = False
        if self.count > space.max_count:
            space.max_count = self.count
        

        return self.death


