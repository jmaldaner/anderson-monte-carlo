import argparse
import pickle
class store(object):
    def __init__(self):
        pass

def get_args(space_default, materials_default, profile_default, pump_default):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-n','--n', 
                        help = 'Number of Photons')
     
    parser.add_argument('-s','--space_det', 
                        default = space_default ,
                        help = 'File that contains the space data')
                        
    parser.add_argument('-fm','--fibre_materials', 
                        default = materials_default, 
                        help = 'File that contains the fibre material data')
                        
    parser.add_argument('-fp','--fibre_profile',
                        default = profile_default,  
                        help = 'File that contains the fibre profile data')
                        
    parser.add_argument('-p','--pump', 
                        default = pump_default, 
                        help = 'Pump Data')
                        
                   
    args = parser.parse_args()
    path = 'classes/config/'
    
    
    space_file = open(path + args.space_det + '.pkl', 'rb')
    material_file = open(path + args.fibre_materials + '.pkl', 'rb')
    profile_file = open(path + args.fibre_profile + '.pkl', 'rb')
    pump_file = open(path + args.pump + '.pkl', 'rb')
    
    
    space_data = pickle.load(space_file)
    material_data = pickle.load(material_file)
    profile_data = pickle.load(profile_file)
    pump_data = pickle.load(pump_file)
    
    return space_data, material_data, profile_data, pump_data, int(args.n)
