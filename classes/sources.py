__author__ = "James Maldaner"

from classes.functions.constants import *
import numpy as np

random = np.random.uniform
gauss = np.random.normal


class Pump(object):
    
    def __init__(self,x=0,y=0, phi = 0, dr = 0, z=0, data = None):
    
        # Pump Properties
        self.wavelength = 523 * nano
        self.pulse_duration = 0.6 * nano
        self.repetitionrate = 50
        self.type = "Pump"
        self.energy = 1*10**-6 #J
        
        
        # Physical Location 
        if data == None:
            self.phi = phi # Maximum launch angle from normal to fibre (rad)
            self.dr = dr # radius of spot size
            self.x0 = x # x location of pump
            self.y0 = y # y location of pump
            self.z0 = z # Distance from fibre face
        else:
            self.phi = data.dphi
            self.dr = data.dr
            self.x0 = data.x0
            self.y0 = data.y0
            self.z0 = data.z
        if x != 0 or y !=0:
            self.x0 = x
            self.y0 = y
 
        self.w0 = self.dr
        self.M2 = 1
        self.zR = pi * self.w0**2 / self.wavelength
        print(self.zR)
        self.wR = self.w0 * np.sqrt(1 + (self.M2 ** 2 ))
        

    def launch(self):
        
        ri =  gauss(0.0,self.w0)
        rf =  gauss(0.0, self.wR)
        # Randomise Launching

        thetapump = 2.0 * pi * random() - pi
        thetarand = 2.0 * pi * random()
        
        xrand = ri * np.cos(thetapump)
        yrand = ri * np.sin(thetapump)
        
        phirand = np.arctan((rf - ri)/self.zR) 

        x = self.x0 + xrand # X launch with random
        y = self.y0 + yrand # Y launch with random
        z = self.z0# + 0.00001
        phi = phirand # Phi (angle from normal) with random (azimuth)
        theta = thetarand # Theta (angle from pointing up) with random (polar)

        return x,y,z,phi, theta
        
class Spontaneous_object(object):
    # rhodamine 640
    # https://www.photonicsolutions.co.uk/upfiles/Rhodamine%20640%20Perchlorate.pdf
    def __init__(self, dye):
        self.wavelength= dye.wavelength_emmit
        self.wavelength_absorb = dye.wavelength_absorb
        self.E_conv = self.wavelength_absorb / self.wavelength
        self.type = "Spontaneous"
        self.photon_energy = c * h / self.wavelength
        self.energy = self.photon_energy
        self.quantumyield = dye.quantumyield
        
        
    
    def launch(self):
        phirand =   pi * random()
        thetarand = 2.0 * pi * random()
        
        phi = phirand
        theta = thetarand
        return self.spont_x,self.spont_y,self.spont_z,phi,theta
        
        
class Stimulated_object(object):
    def __init__(self, dye):
        self.wavelength= dye.wavelength_emmit
        self.type = "Stimulated"
        self.wavelength_absorb = dye.wavelength_absorb
        self.E_conv = self.wavelength_absorb / self.wavelength
        self.photon_energy = c * h / self.wavelength
        self.energy = self.photon_energy
        self.quantumyield = dye.quantumyield
            


        
    def launch(self):
        x = self.stim_x
        y = self.stim_y
        z = self.stim_z
        return self.stim_x,self.stim_y,self.stim_z, self.stim_phi,self.stim_theta
